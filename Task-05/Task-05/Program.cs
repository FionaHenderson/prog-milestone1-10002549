﻿using System;

namespace Task_05
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a time pm on a 24 hour clock to convert to 12 hour time - any time between from 1 to 24");
            var time = int.Parse(Console.ReadLine());

            if (time < 12)
            {
                Console.WriteLine($"The time is {time} am.");
            }
            else if (time == 24)
            {
                Console.WriteLine($"The time is {time - 12} am");
            }
            else if (time > 24)
            {
                Console.WriteLine("Error! Please enter a time between 1 and 24");
            }
            else if (time > 12)
            {
                Console.WriteLine($"The time is {time - 12} pm");
            }
            else if (time == 12)
            {
                Console.WriteLine($"The time is {time} pm");
            }
                   
        }
    }
}
