﻿using System;
using System.Collections.Generic;

namespace Task_22
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var fresh = new Dictionary<string, String>();
            fresh.Add("Apple", "Fruit");
            fresh.Add("Potato", "Vegetable");
            fresh.Add("Carrot", "Vegetable");
            fresh.Add("Orange", "Fruit");
            fresh.Add("Spinach", "Vegetable");
            fresh.Add("Pear", "Fruit");
            fresh.Add("Brocolli", "Vegetable");
            fresh.Add("Mandarin", "Fruit");
            fresh.Add("Kiwifruit", "Fruit");
            fresh.Add("Leek", "Vegetable");
            fresh.Add("Lettuce", "Vegetable");
            fresh.Add("Banana", "Fruit");

            var Fruits = new List<string> { };
            var Vegetables = new List<string> { };

            foreach (var x in fresh)
            
                if (x.Value =="Fruit")
                    {
                    Fruits.Add(x.Key);
                    }
                else
                    {
                    Vegetables.Add(x.Key);
                    }

            Console.WriteLine("There are: {0} Fruits", Fruits.Count);
            Console.WriteLine("There are: {0} Vegetables", Vegetables.Count);
            Console.WriteLine($"The fruits are: {(string.Join(",", Fruits))}");
        }
        
    }
}
