﻿using System;

namespace Task_03
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Did you record you distance in Miles or Kilometres? - Please enter m for Miles or k for Kilometres");

            var metric = Console.ReadLine();
            const double miles2kms = 1.60934;
            const double kms2miles = 0.621371;

            switch (metric)
            {
                case "k":
                    Console.WriteLine("How many Kilometers did you travel? - enter a number.");
                    var getKM = double.Parse(Console.ReadLine());
                    var roundingKM = System.Math.Round(getKM * miles2kms, 2);
                    Console.WriteLine($"You travelled {getKM} Kilometres which is equal to {roundingKM} Miles.");
                    break;
                case "m":
                    Console.WriteLine("How many Miles did you travel? - enter a number");
                    var getMile = double.Parse(Console.ReadLine());
                    var roundingMile = System.Math.Round(getMile * miles2kms, 2);
                    Console.WriteLine($"You travelled {getMile} Miles which is equal to {roundingMile} Kilometres.");
                    break;
                default:
                    Console.WriteLine("Please run the program again and ensure you type in m for Miles or k for kilometres");
                    break;
            }

        }
    }
}
