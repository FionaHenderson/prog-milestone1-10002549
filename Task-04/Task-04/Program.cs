﻿using System;

namespace Task_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Did the recipe ask you to set the oven temperature to Celsius for Farenheit? - Please enter c for Celsius or f for Farenheit");

            var metric = Console.ReadLine();
            
            switch (metric)
            {
                case "c":
                    Console.WriteLine("How many degress Celsius did the recipie tell you to set your oven? - Enter a number.");
                    var getCelsius = double.Parse(Console.ReadLine());
                    var roundingCelsius = System.Math.Round(getCelsius * 9 / 5 + 32, 2);
                    Console.WriteLine($"The recepie wants yo to heat your oven to {getCelsius} Degrees Celsius which is equal to {roundingCelsius} Degrees Farenheit.");
                    break;
                case "f":
                    Console.WriteLine("How many degrees Farenheit did the recepie tell you to set your oven? - Enter a number.");
                    var getFarenheit = double.Parse(Console.ReadLine());
                    var roundingFarenheit = System.Math.Round((getFarenheit - 32) * 5 / 9, 2);
                    Console.WriteLine($"The recepie wants you to heat your oven to {getFarenheit} Degrees Farenheit which is equal to {roundingFarenheit} Degrees Celsius.");
                    break;
                default:
                    Console.WriteLine("Please run the program again and ensure you type in c for Celsius or f for Farenheit");
                    break;
            }
        }
    }
}
