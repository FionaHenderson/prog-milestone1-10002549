﻿using System;

namespace Task_14
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter <tb> to convert TB to GB or <gb> to convert GB to TB");
            var metric = Console.ReadLine();
            const double tb2gb = 1024;
            const double gb2tb = 0.0009765625;

            switch(metric)
            {
                case "tb":
                    Console.WriteLine("Enter your storage size in TB to convert to GB");
                    var getGB = double.Parse(Console.ReadLine());
                    var roundingGB = System.Math.Round(getGB * tb2gb, 2);
                    Console.WriteLine($"{getGB} TBs is {roundingGB} GBs");
                    break;
                case "gb":
                    Console.WriteLine("Enter your storage size in GB to convert to TB");
                    var getTB = double.Parse(Console.ReadLine());
                    var roundingTB = System.Math.Round(getTB * gb2tb, 2);
                    Console.WriteLine($"{getTB} GBs is {roundingTB} TBs");
                    break;
                default:
                    Console.WriteLine("Invalid entry! Please run the program again and type in tb for Terabytes or gb for Gigabytes");
                    break;
            }
        }
    }
}
