﻿using System;

namespace Task_09
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int i = 2016;
            var counter = 5;
            var index = 0;

            for (index = 0; index < counter; index++)
            {
                Console.WriteLine($"The next leap year is in {i += 4}");
            }           
        }
    }
}
