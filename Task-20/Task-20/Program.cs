﻿using System;
using System.Collections.Generic;

namespace Task_20
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 33, 45, 21, 44, 67, 87, 86 };
            List<int> even = new List<int>();
            List<int> odd = new List<int>();

            foreach (int number in numbers)
            {
                if (number % 2 == 0)
                {
                    even.Add(number);
                }
                else
                {
                    odd.Add(number);                  
                }                
            }
            Console.WriteLine($"The odd numbers from the array of numbers are {(String.Join(",", odd))}");
        }
    }
}
