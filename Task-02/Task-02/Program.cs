﻿using System;

namespace Task_02
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var month = "";
            Console.WriteLine("What month were you born?");
            month = Console.ReadLine();

            var day = 0;
            Console.WriteLine($"What date in {month} were you born?");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine($"Thanks. I will add your birthday on {day} {month} to my calendar.");
        }
    }
}
