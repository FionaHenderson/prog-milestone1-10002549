﻿using System;

namespace Task_24
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var menu = "M";

            do
            {
                Console.Clear();

                Console.WriteLine("*********************************");
                Console.WriteLine("*           MAIN MENU           *");
                Console.WriteLine("*                               *");
                Console.WriteLine("*        1. Option One          *");
                Console.WriteLine("*        2. Option Two          *");
                Console.WriteLine("*        3. Option Three        *");
                Console.WriteLine("*        4. Option Four         *");
                Console.WriteLine("*        5. Option Five         *");
                Console.WriteLine("*                               *");
                Console.WriteLine("*********************************");
                Console.WriteLine();
                Console.WriteLine("Please Select an Option from the Menu");
                menu = (Console.ReadLine());

                if (menu == "1")
                {
                    Console.Clear();
                    Console.WriteLine("This page is still under construction.");
                    Console.WriteLine("");
                    Console.WriteLine("Please type in <M> to return to the MAIN MENU");
                    menu = (Console.ReadLine());
                }
                if (menu == "2")
                {
                    Console.Clear();
                    Console.WriteLine("This page is still under construction.");
                    Console.WriteLine("");
                    Console.WriteLine("Please type in <M> to return to the MAIN MENU");
                    menu = (Console.ReadLine());
                }
                if (menu == "3")
                {
                    Console.Clear();
                    Console.WriteLine("This page is still under construction.");
                    Console.WriteLine("");
                    Console.WriteLine("Please type in <M> to return to the MAIN MENU");
                    menu = (Console.ReadLine());
                }
                if (menu == "4")
                {
                    Console.Clear();
                    Console.WriteLine("This page is still under construction.");
                    Console.WriteLine("");
                    Console.WriteLine("Please type in <M> to return to the MAIN MENU");
                    menu = (Console.ReadLine());
                }
                if (menu == "5")
                {
                    Console.Clear();
                    Console.WriteLine("This page is still under construction.");
                    Console.WriteLine("");
                    Console.WriteLine("Please type in <M> to return to the MAIN MENU");
                    menu = (Console.ReadLine());               
                }
                else 
                {
                    Console.Clear();
                    Console.WriteLine("You did not select an item from the menu. Please type in <M> to return to the Main Menu");
                    menu = (Console.ReadLine());
                }
            } while (menu == "M");                                     
        }
    }
}
