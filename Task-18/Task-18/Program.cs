﻿using System;
using System.Collections.Generic;

namespace Task_18
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string,
            int>>();

            names.Add(Tuple.Create("Melissa, August", 15));
            names.Add(Tuple.Create("Adam, Jaunary", 05));
            names.Add(Tuple.Create("Fred, October", 27));

            Console.WriteLine(string.Join(", ", names));
        }
    }
}
