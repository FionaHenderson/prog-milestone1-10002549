﻿using System;

namespace Task_01
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var name = "";
            Console.WriteLine("Hello what is your name");
            name = Console.ReadLine();

            var age = 0;
            Console.WriteLine("What is your age");
            age = int.Parse(Console.ReadLine());

            Console.WriteLine("Hello " + name + " you are " + age + " years old.");
            Console.WriteLine("Hello {0} you are {1} years old.", name, age);
            Console.WriteLine($"Hello {name} you are {age} years old.");
        }
    }
}
