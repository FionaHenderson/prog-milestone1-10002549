﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter 5 numbers to find the sum of all numbers");

            var n = new List<string>();            
            string input = "";
            var counter = 5;
            var i = 0;

            do
            {
                Console.WriteLine("Enter a number");
                input = Console.ReadLine();
                n.Add(input);

                i++;
            }
            while (i < counter);

            int result = 0;

            foreach (string s in n)
            {
                result = result + int.Parse(s);
            }
            Console.WriteLine($"The sum of {(string.Join(", ", n))} = {result}");

        }
    }
}
