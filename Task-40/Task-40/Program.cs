﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> Day = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
            List<int> NoOfWed = new List<int>();
            int i = 0;
            var correct = true;

            for (i = 3; i <= 31; i += 7) 
            {
                if (correct)
                {
                    NoOfWed.Add(i);
                }                               
            }
            Console.WriteLine($"The number of Wednesday's in the Month of October is {(NoOfWed.Count)}.");
        }
    }
}
