﻿using System;
using System.Collections.Generic;

namespace Task_17
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var names = new List<Tuple<string,
            int>>();

            names.Add(Tuple.Create("Melissa", 28));
            names.Add(Tuple.Create("Adam", 30));
            names.Add(Tuple.Create("Fred", 29));

            Console.WriteLine(string.Join(", ", names));
        }
    }
}
