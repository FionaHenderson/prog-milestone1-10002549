﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the amount for your 3 purchases in dollars and cents and press the enter key");

            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());
            var c = double.Parse(Console.ReadLine());
            const double gst = 1.15;
            var answer = Math.Round((a + b + c) * gst, 2);

            Console.WriteLine($"The total amount to pay for your purchases of ${a}, ${b} and ${c} + 15% GST is ${answer}");
        }
    }
}
