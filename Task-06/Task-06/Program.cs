﻿using System;

namespace Task_06
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter 5 numbers to sum them up: Enter any number to begin.");
                       
            double n = double.Parse(Console.ReadLine());
            double r;
            double sum = 0;
            
            for (int i = 0; i < n; i++)
            {
                    Console.WriteLine("Please enter a number");
                    r = double.Parse(Console.ReadLine());

                    sum += r;
                    Console.WriteLine($"The sum of your numbers = {sum}");                                  
            }
        }            
    }
}
