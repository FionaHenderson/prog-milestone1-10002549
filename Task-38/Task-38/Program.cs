﻿using System;

namespace Task_38
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int counter = 13;
            int i = 0;

            for (i = 1; i < counter; i++)
            {
                int a = i * 9;
                int b = a / 9;
                Console.WriteLine($"{a} Divided by 9 = {b}");
            } 
        }
    }
}
