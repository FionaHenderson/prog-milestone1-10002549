﻿using System;

namespace Task_30
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number");
            var nbr1 = Console.ReadLine();

            Console.WriteLine("Please enter another number");
            var nbr2 = Console.ReadLine();

            var answer = (nbr1 + nbr2);

            Console.WriteLine($"The answer to the String {nbr1} + {nbr2} = {answer}.");

            Console.WriteLine();
            Console.WriteLine("*****************************************************");
            Console.WriteLine();

            Console.WriteLine("Please enter a number");
            int nbr3 = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter another number");
            int nbr4 = int.Parse(Console.ReadLine());

            var answer2 = (nbr3 + nbr4);

            Console.WriteLine($"The answer to the Integers {nbr3} + {nbr4} = {answer2}.");
        }
    }
}
