﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_11
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int year;
            Console.WriteLine("Enter a year to see if it is a Leap Year");
            year = int.Parse(Console.ReadLine());

            if (year % 400 == 0)
            {
                Console.WriteLine("Yes", year);
            }
            else if (year % 100 == 0)
            {
                Console.WriteLine("No", year);
            }
            else if (year % 4 == 0)
            {
                Console.WriteLine("Yes", year);
            }
            else
                Console.WriteLine("No", year);
        }
    }
}
